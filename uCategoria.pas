unit uCategoria;

interface

uses
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait, Classes, SysUtils,
  uPrincipal;

type
  TCategoria = class(TObject)
  private
    FId_categoria: Integer;
    FEtapa_id: Integer;
    FModalidade_id: Integer;
    FDescricaocat: String;
    FCodcategoriacat: Integer;
    FIdadeinicat: Integer;
    FIdadefimcat: Integer;
    FSexocat: String;
    FQtdeatletacat: Integer;
    FTCategoriaQuery: TFDQuery;
  public
    constructor Create;
    destructor Destroy; override;

    procedure MeuAfterOpen(DataSet: TDataSet);

    property Id_categoria: Integer read FId_categoria write FId_categoria;
    property Etapa_id: Integer read FEtapa_id write FEtapa_id;
    property Modalidade_id: Integer read FModalidade_id write FModalidade_id;
    property Descricaocat: String read FDescricaocat write FDescricaocat;
    property Codcategoriacat: Integer read FCodcategoriacat write FCodcategoriacat;
    property Idadeinicat: Integer read FIdadeinicat write FIdadeinicat;
    property Idadefimcat: Integer read FIdadefimcat write FIdadefimcat;
    property Sexocat: String read FSexocat write FSexocat;
    property Qtdeatletacat: Integer read FQtdeatletacat write FQtdeatletacat;
    property TCategoriaQuery: TFDQuery read FTCategoriaQuery write FTCategoriaQuery;
end;

implementation

{ TCategoria }

constructor TCategoria.Create;
var
  lSql : TStringList;
begin
  FTCategoriaQuery :=  TFDQuery.Create(nil);
  FTCategoriaQuery.AfterOpen := MeuAfterOpen;
  lSql := TStringList.Create;
  try
    lSql.Add('select * from categoria');
    FTCategoriaQuery.Connection := Form1.PgConnection;
    FTCategoriaQuery.SQL := lSql;
  finally
    lSql.Free;
  end;
end;

destructor TCategoria.Destroy;
begin
  FreeAndNil(FTCategoriaQuery);
  inherited;
end;

procedure TCategoria.MeuAfterOpen(DataSet: TDataSet);
begin
  DataSet.FieldList[0].DisplayLabel := 'ID_Categoria';
  DataSet.FieldList[1].DisplayLabel := 'Etapa_ID';
  DataSet.FieldList[2].DisplayLabel := 'Modalidade_ID';
  DataSet.FieldList[3].DisplayLabel := 'Descri��o Categoria';
  DataSet.FieldList[4].DisplayLabel := 'C�digo Categoria';
  DataSet.FieldList[5].DisplayLabel := 'Idade Inicio';
  DataSet.FieldList[6].DisplayLabel := 'Idade Fim';
  DataSet.FieldList[7].DisplayLabel := 'Sexo';
  DataSet.FieldList[8].DisplayLabel := 'Qtde Atletas';
end;

end.

