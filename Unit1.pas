unit uCategoria;

interface

type
  TCategoria = class(TObject)
  private
    FId_categoria: Integer;
    FEtapa_id: Integer;
    FModalidade_id: Integer;
    FDescricaocat: String;
    FCodcategoriacat: Integer;
    FIdadeinicat: Integer;
    FIdadefimcat: Integer;
    FSexocat: String;
    FQtdeatletacat: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    property Id_categoria: Integer read FId_categoria write FId_categoria;
    property Etapa_id: Integer read FEtapa_id write FEtapa_id;
    property Modalidade_id: Integer read FModalidade_id write FModalidade_id;
    property Descricaocat: String read FDescricaocat write FDescricaocat;
    property Codcategoriacat: Integer read FCodcategoriacat write FCodcategoriacat;
    property Idadeinicat: Integer read FIdadeinicat write FIdadeinicat;
    property Idadefimcat: Integer read FIdadefimcat write FIdadefimcat;
    property Sexocat: String read FSexocat write FSexocat;
    property Qtdeatletacat: Integer read FQtdeatletacat write FQtdeatletacat;
end;

implementation

{ TCategoria }


constructor TCategoria.Create;
begin

end;

destructor TCategoria.Destroy;
begin

  inherited;
end;

end.

