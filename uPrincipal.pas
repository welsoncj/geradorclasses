unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Phys.PGDef, FireDAC.Phys.PG, Data.DB, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.ComCtrls, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    PgConnection: TFDConnection;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    DBGrid1: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    ListBox2: TListBox;
    ListBox1: TListBox;
    FDMetaInfoQuery1: TFDMetaInfoQuery;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Button1: TButton;
    FDQuery1: TFDQuery;
    procedure FormShow(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    function PrimeiraLetraMaiuscula(APalavra: String):String;
    procedure AdicionarConstructor(AMemo:TMemo);
    procedure AdicionarDestructor(AMemo:TMemo);
    procedure AdicionaImplementation(AMemo:TMemo; ANomeClasse:String);
    procedure AdicionaCreate(AMemo:TMemo; ANomeTabela:String);
    procedure AdicionaDestroy(AMemo:TMemo);
    procedure AdicionaFim(AMemo:TMemo);
    procedure AdicionarUses(AMemo:TMemo);
    procedure AdicionaFieldQuery(AMemo:TMemo; ANomeClasse:String);
    procedure AdicionaPropertyQuery(AMemo:TMemo; ANomeClasse:String);
    procedure AdicionaProcedures(AMemo:TMemo);
    procedure AdicionarImplementacaoProcedures(AMemo:TMemo; ANomeClasse, ANomeTabela:String);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  StrUtils, unitteste;

{$R *.dfm}

procedure TForm1.AdicionaCreate(AMemo: TMemo; ANomeTabela: String);
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('constructor TCategoria.Create;');
  AMemo.Lines.Add('var');
  AMemo.Lines.Add('  lSql : TStringList;');
  AMemo.Lines.Add('begin');
  AMemo.Lines.Add('  FTCategoriaQuery :=  TFDQuery.Create(nil);');
  AMemo.Lines.Add('  FTCategoriaQuery.AfterOpen := MeuAfterOpen;');
  AMemo.Lines.Add('  lSql := TStringList.Create;');
  AMemo.Lines.Add('  try');
  AMemo.Lines.Add('    lSql.Add('+ QuotedStr('select * from ' + ANomeTabela + '') + ');');
  AMemo.Lines.Add('    FTCategoriaQuery.Connection := Form1.PgConnection;');
  AMemo.Lines.Add('    FTCategoriaQuery.SQL := lSql;');
  AMemo.Lines.Add('  finally');
  AMemo.Lines.Add('    lSql.Free;');
  AMemo.Lines.Add('  end;');
  AMemo.Lines.Add('end;');
end;

procedure TForm1.AdicionaDestroy(AMemo: TMemo);
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('destructor TCategoria.Destroy;');
  AMemo.Lines.Add('begin');
  AMemo.Lines.Add('  FreeAndNil(FTCategoriaQuery);');
  AMemo.Lines.Add('  inherited;');
  AMemo.Lines.Add('end;');
end;

procedure TForm1.AdicionaFieldQuery(AMemo: TMemo; ANomeClasse:String);
begin
  AMemo.Lines.Add('    F' + ANomeClasse + 'Query: TFDQuery;');
end;

procedure TForm1.AdicionaFim(AMemo: TMemo);
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('end.');
end;

procedure TForm1.AdicionaImplementation(AMemo: TMemo; ANomeClasse:String);
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('implementation');
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('{ ' + ANomeClasse + ' }');
end;

procedure TForm1.AdicionaProcedures(AMemo:TMemo);
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('    procedure MeuAfterOpen(DataSet: TDataSet);');
  AMemo.Lines.Add(' ');
end;

procedure TForm1.AdicionaPropertyQuery(AMemo: TMemo; ANomeClasse: String);
begin
  AMemo.Lines.Add('    property ' + ANomeClasse + 'Query' + ': ' + 'TFDQuery read F' + ANomeClasse + 'Query' + ' write F' + ANomeClasse + 'Query' + ';');
end;

procedure TForm1.AdicionarConstructor(AMemo: TMemo);
begin
  AMemo.Lines.Add('    constructor Create;');
end;

procedure TForm1.AdicionarDestructor(AMemo: TMemo);
begin
  AMemo.Lines.Add('    destructor Destroy; override;');
end;

procedure TForm1.AdicionarImplementacaoProcedures(AMemo: TMemo; ANomeClasse, ANomeTabela:String);
  procedure GetDisplayFields;
  begin
    FDQuery1.Close;
    FDQuery1.ParamByName('BANCO_DADOS').AsString :=  ListBox1.Items[ListBox1.ItemIndex];
    FDQuery1.ParamByName('SCHEMA').AsString := ListBox2.Items[ListBox2.ItemIndex];
    FDQuery1.ParamByName('TABELA').AsString := ANomeTabela;
    FDQuery1.Open;

    while not FDQuery1.Eof do
    begin
      if FDQuery1.FieldByName('column_comment').AsString <> '' then
        AMemo.Lines.Add('  DataSet.FieldList[' + IntToStr(FDQuery1.RecNo - 1) + '].DisplayLabel := ' + QuotedStr(FDQuery1.FieldByName('column_comment').AsString) + ';');
      FDQuery1.Next;
    end;
  end;
begin
  AMemo.Lines.Add(' ');
  AMemo.Lines.Add('procedure ' + ANomeClasse + '.MeuAfterOpen(DataSet: TDataSet);');
  AMemo.Lines.Add('begin');
  GetDisplayFields;
  AMemo.Lines.Add('end;');
end;

procedure TForm1.AdicionarUses(AMemo: TMemo);
begin
    AMemo.Lines.Add(' ');
    AMemo.Lines.Add('uses');
    AMemo.Lines.Add('  FireDAC.Stan.Intf, FireDAC.Stan.Option,');
    AMemo.Lines.Add('  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,');
    AMemo.Lines.Add('  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,');
    AMemo.Lines.Add('  FireDAC.Comp.DataSet, FireDAC.Comp.Client,FireDAC.UI.Intf, FireDAC.Stan.Def,');
    AMemo.Lines.Add('  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.VCLUI.Wait, Classes, SysUtils,');
    AMemo.Lines.Add('  uPrincipal;');
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  lForm: TForm2;
begin
  lForm :=  TForm2.Create(Self);
  try
    lForm.ShowModal;
  finally
    FreeAndNil(lForm);
  end;
end;

procedure TForm1.FormShow(Sender: TObject);
var
  lListaDB: TStringList;
  I: Integer;
begin
  ListBox1.Clear;
  lListaDB := TStringList.Create;
  try
    PgConnection.GetCatalogNames('', ListBox1.Items);
  finally
    lListaDB.Free;
  end;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var
  lListaDB: TStringList;
  i: Integer;
begin
  Memo1.Lines.Clear;
  lListaDB := TStringList.Create;
  try
    PgConnection.GetSchemaNames(ListBox1.Items[ListBox1.ItemIndex],'', ListBox2.Items);
  finally
    lListaDB.Free;
  end;
end;

procedure TForm1.ListBox2Click(Sender: TObject);
var
  lListaDB, lListaFields, lListaProperty: TStringList;
  i: Integer;
  lTabSheet: TTabSheet;
  lMemo: Tmemo;
  lNomeClasse: String;
  lNomeTabela: string;
begin
  lListaDB := TStringList.Create;
  try
    PgConnection.Connected := False;
    PgConnection.Params.Add('Database='+ListBox1.Items[ListBox1.ItemIndex]);
    PgConnection.Connected := True;

    PgConnection.GetTableNames ( ListBox1.Items[ListBox1.ItemIndex] ,  ListBox2.Items[ListBox2.ItemIndex] ,  '' , lListaDB ) ;
    Memo1.Text:= lListaDB.Text;

    for I := 0 to lListaDB.Count -1 do
    begin
      lTabSheet := TTabSheet.Create(PageControl1);
      lTabSheet.PageControl := PageControl1;
      lTabSheet.Name := lListaDB.Strings[I];
      lTabSheet.Caption :=lListaDB.Strings[I];

      lMemo := TMemo.Create(lTabSheet);
      lMemo.Parent := lTabSheet;
      lMemo.Align := alClient;
      lMemo.ScrollBars := ssVertical;

      lMemo.Lines.Add('unit u'+ PrimeiraLetraMaiuscula(lTabSheet.Name) + ';');
      lMemo.Lines.Add(' ');
      lMemo.Lines.Add('interface');
      AdicionarUses(lMemo);
      lMemo.Lines.Add(' ');

      lMemo.Lines.Add('type');
      lNomeClasse := 'T' + PrimeiraLetraMaiuscula(lTabSheet.Name);
      lNomeTabela := lTabSheet.Name;
      lMemo.Lines.Add('  ' + lNomeClasse + ' = class(TObject)');

      lMemo.Lines.Add('  private');

      FDMetaInfoQuery1.Close;
      FDMetaInfoQuery1.CatalogName := ListBox1.Items[ListBox1.ItemIndex];
      FDMetaInfoQuery1.SchemaName := ListBox2.Items[ListBox2.ItemIndex];
      FDMetaInfoQuery1.ObjectName := lTabSheet.Name;
      FDMetaInfoQuery1.Open;

      FDMetaInfoQuery1.First;
      FDMetaInfoQuery1.DisableControls;
      lListaFields := TStringList.Create;
      lListaProperty := TStringList.Create;
      try
        while not FDMetaInfoQuery1.Eof do
        begin
          //lmemo.Lines.Add('    F' + FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString);
          case FDMetaInfoQuery1.FieldByName('COLUMN_DATATYPE').AsInteger of
            4  :
            begin
              lListaFields.Add('    F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': Integer;');
              lListaProperty.Add('    property ' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': ' + 'Integer read F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ' write F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ';');
            end;
            17 :
            begin
              lListaFields.Add('    F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': TDateTime;');
              lListaProperty.Add('    property ' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': ' + 'TDateTime read F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ' write F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ';');
            end;
            18 :
            begin
              lListaFields.Add('    F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': TDate;');
              lListaProperty.Add('    property ' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': ' + 'TDate read F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ' write F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ';');
            end;
            24 :
            begin
              lListaFields.Add('    F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': String;');
              lListaProperty.Add('    property ' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ': ' + 'String read F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ' write F' + PrimeiraLetraMaiuscula(FDMetaInfoQuery1.FieldByName('COLUMN_NAME').AsString) + ';');
            end;
          end;
          FDMetaInfoQuery1.Next
        end;

        lMemo.Text := lMemo.Text + #13 + lListaFields.Text;
        AdicionaFieldQuery(lMemo, lNomeClasse);
        lMemo.Lines.Add('  public');
        AdicionarConstructor(lMemo);
        AdicionarDestructor(lMemo);
        AdicionaProcedures(lMemo);
        lMemo.Text := lMemo.Text + #13 + lListaProperty.Text;
        AdicionaPropertyQuery(lMemo, lNomeClasse);

      finally
        lListaFields.Free;
        lListaProperty.Free;
      end;
      FDMetaInfoQuery1.EnableControls;
      lMemo.Lines.Add('end;');
      AdicionaImplementation(lMemo, lNomeClasse);
      AdicionaCreate(lMemo, lNomeTabela);
      AdicionaDestroy(lMemo);
      AdicionarImplementacaoProcedures(lMemo, lNomeClasse, lNomeTabela);
      AdicionaFim(lMemo);
    end;
  finally
    lListaDB.Free;
  end;
end;

function TForm1.PrimeiraLetraMaiuscula(APalavra: String): String;
begin
  Result := UpperCase(LeftStr(APalavra,1)) + RightStr(APalavra, Length(APalavra) - 1);
end;

end.
