object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 527
  ClientWidth = 855
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 400
    Top = 192
    Width = 320
    Height = 120
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 855
    Height = 486
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Principal'
      object Memo1: TMemo
        Left = 306
        Top = 0
        Width = 541
        Height = 458
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object ListBox2: TListBox
        Left = 153
        Top = 0
        Width = 153
        Height = 458
        Align = alLeft
        ItemHeight = 13
        TabOrder = 1
        OnClick = ListBox2Click
      end
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 153
        Height = 458
        Align = alLeft
        ItemHeight = 13
        TabOrder = 2
        OnClick = ListBox1Click
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 855
    Height = 41
    Align = alTop
    TabOrder = 2
    object Button1: TButton
      Left = 157
      Top = 10
      Width = 124
      Height = 25
      Caption = 'Abrir Form Teste'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object PgConnection: TFDConnection
    Params.Strings = (
      'User_Name=postgres'
      'Password=postgres'
      'Database=riscozerocrono'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evMode, evRecsSkip, evRecsMax, evUnidirectional]
    Connected = True
    LoginPrompt = False
    Left = 53
    Top = 56
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    VendorHome = 'C:\GeradorClasses'
    Left = 144
    Top = 152
  end
  object FDMetaInfoQuery1: TFDMetaInfoQuery
    Connection = PgConnection
    FormatOptions.AssignedValues = [fvQuoteIdentifiers, fvCheckPrecision]
    FormatOptions.CheckPrecision = True
    FormatOptions.QuoteIdentifiers = True
    MetaInfoKind = mkTableFields
    TableKinds = [tkSynonym, tkTable, tkView, tkTempTable, tkLocalTable]
    ObjectScopes = [osMy, osOther, osSystem]
    Left = 400
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = FDMetaInfoQuery1
    Left = 544
    Top = 40
  end
  object FDQuery1: TFDQuery
    Connection = PgConnection
    SQL.Strings = (
      'SELECT'
      '    cols.column_name,'
      '    ('
      '        SELECT'
      
        '            pg_catalog.col_description(c.oid, cols.ordinal_posit' +
        'ion::int)'
      '        FROM pg_catalog.pg_class c'
      '        WHERE'
      
        '            c.oid     = (SELECT cols.table_name::regclass::oid) ' +
        'AND'
      '            c.relname = cols.table_name'
      '    ) as column_comment'
      ' '
      'FROM information_schema.columns cols'
      'WHERE'
      '    cols.table_catalog = :banco_dados'
      '    AND cols.table_schema  = :schema '
      '    AND cols.table_name    = :tabela'
      'order by cols.ordinal_position')
    Left = 368
    Top = 152
    ParamData = <
      item
        Name = 'BANCO_DADOS'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'SCHEMA'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'TABELA'
        DataType = ftString
        ParamType = ptInput
      end>
  end
end
